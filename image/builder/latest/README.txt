To include Android SDK, create image based on latest container

run --name android-builder -ti registry.gitlab.com/promethistai/system/builder:latest bash
wget https://repository.flowstorm.ai/dist/android-commandlinetools-linux_latest.zip
cd /opt && unzip -r /android-commandlinetools-linux_latest.zip
mv cmdline-tools android-sdk
/opt/android-sdk/bin/sdkmanager --licenses --sdk_root=/opt/android-sdk