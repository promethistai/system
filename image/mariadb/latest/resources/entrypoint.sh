#!/bin/bash
chown -R mysql:mysql /var/lib/mysql

if [ ! -z "${INIT}" ] && [ "${INIT}" = "TRUE" ] && [ ! -f /var/lib/mysql/.init_done ] && [ ! -z "${DB_NAME}" ] && [ ! -z "${DB_USER}" ]; then
    echo "Initialisation started!"
    service mysql start

    if [ -z "${DB_PWD}" ]; then
        echo "DB_PWD not set - generating random one"
        DB_PWD=`cat /dev/urandom | tr -dc A-Za-z0-9 | head -c24`
        echo $DB_PWD > /var/lib/mysql/.dbpasswd
        chmod 400 /var/lib/mysql/.dbpasswd
    fi

#    mysql -e "INSTALL SONAME 'server_audit'"
#    mysql -e "INSTALL SONAME 'auth_pam'"
#    mysql -e "CREATE USER $DB_USER IDENTIFIED VIA pam USING 'mariadb'"

    echo "Creating user $DB_USER..."
    mysql -e "CREATE USER '$DB_USER'@'%' IDENTIFIED by '$DB_PWD'"

    echo "Creating database $DB_NAME..."
    mysql -e "CREATE DATABASE $DB_NAME"
    mysql -e "GRANT ALL PRIVILEGES ON $DB_NAME.* TO $DB_USER"

    count=`ls -1 /initdb.d/ 2>/dev/null | wc -l`
    if [ $count != 0 ]; then
        for f in /initdb.d/*; do
            case "$f" in
                *.sh)           echo "$0: running $f"; . "$f" ;;
                *.sql|*.dmp)    echo "$0: running $f"; mysql $DB_NAME < "$f"; echo ;;
                *.sql.gz)       echo "$0: running $f"; gunzip -c "$f" | mysql $DB_NAME; echo ;;
                *)              echo "$0: ignoring $f" ;;
            esac
            echo
        done
    fi
    touch /var/lib/mysql/.init_done
    service mysql stop

    echo "Initialisation completed!"
    echo "###############################"

fi

unset DB_PWD #unset variable for security reasons

# if command starts with an option, prepend mysqld
if [ "${1:0:1}" = '-' ]; then
	set -- gosu mysql mysqld "$@"
fi


exec "$@"
