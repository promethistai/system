<?php
$defaultKey = '60353867bbc21e2bef56faaf';
$defaultInput = file_get_contents('input.txt');
$defaultOutput = file_get_contents('output.txt');

if ($_GET['run'] || ($_POST['key'] && $_POST['input'] && $_POST['output'])) {
    $key = $_POST['key'] ?: $defaultKey;
    $input = $_POST['input'] ?: $defaultInput;
    $output = str_replace(["\r\n", "\r", "\n"], "\n", trim($_POST['output'] ?: $defaultOutput));
    $inputFile = uniqid('/tmp/');
    $outputFile = uniqid('/tmp/');
    $expectedOutputFile = uniqid('/tmp/');
    file_put_contents($inputFile, $input);
    file_put_contents($expectedOutputFile, $output);
    $cmd = "/opt/jdk14/bin/java -jar /opt/flowstorm-client-app-1.0.0-SNAPSHOT-all.jar client -L WARN -k $key -ct HttpSocket -nia -noa -i $inputFile -o $outputFile";
    //echo "<pre>$cmd</pre>";
    $res = exec($cmd, $out, $err);
    if ($res === false) {
        echo "ERROR $err";
    } else {
        $outputContent = trim(file_get_contents($outputFile));
        if ($outputContent != $output) {
            echo "ERROR NOT MATCHING OUTPUT outputFile=$outputFile(" . strlen($outputContent) . "), expectedOutputFile=$expectedOutputFile(" . strlen($output) . ")";
            echo "<pre>";
            passthru("/usr/bin/diff $outputFile $expectedOutputFile");
            echo "</pre>";
        } else {
            echo "OK";
        }
    }
}
?>
<form action="index.php" method="POST">
    Key<br/><input name="key" value="<?=$_POST['key'] ?: $defaultKey?>"/><br/>
    Input<br/><textarea name="input" cols="80" rows="10"><?=$_POST['input'] ?: $defaultInput?></textarea><br/>
    Expected Output<br/><textarea name="output" cols="80" rows="10"><?=$_POST['output'] ?: $defaultOutput?></textarea><br/>
    <input type="submit" value="Run"/>
</form>
