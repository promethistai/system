#!/bin/bash

if [ "${SENDMAIL}" == "true" ]; then
  service sendmail start
fi

service cron start

exec "$@"
