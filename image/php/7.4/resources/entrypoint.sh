#!/bin/bash

if [ "${SENDMAIL}" == "true" ]; then
  service sendmail start
fi

if test -f "${ENTRYHOOK_FILE}"; then
    . $ENTRYHOOK_FILE
fi

service cron start

exec "$@"
