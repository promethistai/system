echo "username=$STORAGE_ACCOUNT" >> /smb.cred
echo "password=$STORAGE_KEY" >> /smb.cred
mkdir -p $MNT_POINT
echo "//$STORAGE_URL $MNT_POINT cifs nofail,credentials=/smb.cred,dir_mode=0777,file_mode=0777,serverino,nosharesock,actimeo=30" >> /etc/fstab
mount -a && tail -f /dev/null