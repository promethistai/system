#!/bin/bash

set -x

export TZ=Europe/Prague
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
export LC_ALL="C.UTF-8"
export LANG="C.UTF-8"

DEBIAN_FRONTEND=noninteractive \
apt-get update && apt-get install --no-install-recommends -yqq \
procps \
locales \
vim \
less \
rsync \
curl \
wget \
lynx \
sudo \
zip \
unzip \
bzip2 \
ncdu \
lsof \
htop \
iftop \
iotop \
telnet \
tcpdump \
dnsutils \
net-tools \
iputils-ping \
ca-certificates \
apt-transport-https \
lsb-release \
netcat \
|| apt-get clean; \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* /var/log/* /var/cache/*


ln -snf /etc/locale.alias /usr/share/locale/locale.alias
echo "cs_CZ.UTF-8 UTF-8" > /etc/locale.gen
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
