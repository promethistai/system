#!/bin/bash
helm repo update
helm upgrade --install gitlab gitlab/gitlab \
  --timeout 600 \
  --set global.hosts.domain=ci.promethist.ai \
  --set global.hosts.externalIP=35.198.128.7 \
  --set certmanager-issuer.email=admin@promethist.ai