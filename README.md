This project of PromethistAI organization contains common Docker images and Helm charts used for build and and deployment of various cloud services. 
## Kubernetes automated deployment setup
### Admin (running under gcloud project owner's Google ID)

cloud project setup
```
gcloud init
gcloud config set project <projectname>
gcloud container clusters get-credentials <clustername> --region europe-west3-a
kubectl config get-contexts
kubectl config use-context <contextname>

# create other namespaces (if necessary)
kubectl create namespace develop
kubectl create namespace preview

```

Private docker registry secret (create local/docker-config.json first - see https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/ )
```
kubectl create (-n <namespace>) secret generic promethistai-registry --from-file=.dockerconfigjson=local/docker-config.json --type=kubernetes.io/dockerconfigjson
```

nginx-ingress + cert-manager
```

# using ingress-nginx (provided by NGINX - CURRENTLY WE NOT USE IT)
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
helm install nginx-ingress nginx-stable/nginx-ingress --set controller.service.loadBalancerIP=35.198.81.12 --set rbac.create=true --set controller.publishService.enabled=true

# using kubernetes-ingress (provided by Kubernetes)
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
kubectl apply -f deploy/nginx-config.yaml # SSL cipher order setup update (stronger first - Alexa requires it)
helm install ingress-nginx ingress-nginx/ingress-nginx --set controller.service.loadBalancerIP=35.198.81.12 --set rbac.create=true --set controller.publishService.enabled=true


helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl create ns cert-manager
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.6.1 --set installCRDs=true

kubectl apply -f deploy/clusterissuer-letsencrypt.yaml 

```

gateway 
```
helm upgrade --install gateway deploy/gateway
helm upgrade --install --namespace develop --set basedomain=develop.promethist.ai gateway deploy/gateway
helm upgrade --install --set replicaCount=2 --set basedomain=promethist.ai gateway deploy/gateway
```

mysql
```
helm upgrade --install --namespace default --set dataPath=/data/local/default/mariadb mariadb deploy/mariadb
helm upgrade --install --namespace remax --set dataPath=/data/local/remax/mariadb --set service.nodePort=30007 mariadb deploy/mariadb
helm upgrade --install --namespace my --set dataPath=/data/local/my/mariadb --set service.nodePort=30008 mariadb deploy/mariadb
```

NFS server (TODO replace with glusterfs)
```
helm install deploy/nfs-server --name nfs-server
```

### Deployer part
CI pipeline deploy script running in registry.gitlab.com/promethistai/system/deployer container
```
gcloud auth activate-service-account --key-file=${GCP_SA_KEY} --project=${GCP_PROJECT}
gcloud container clusters get-credentials ${GCP_CLUSTER} --region ${GCP_REGION}
helm upgrade --install release-name ...
```

## How to's
 
```
# how to delete terminating persistentvolume
kubectl -n develop patch pv gateway -p '{"metadata":{"finalizers":null}}'

# mariadb administration
kubectl exec -ti mariadb-0 -- mysql -uroot mysql
```